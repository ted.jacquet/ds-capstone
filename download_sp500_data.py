# import modules
import csv
import pandas as pd
from datetime import datetime
#from yahoofinancials import YahooFinancials
import yfinance as yf
import matplotlib.pyplot as plt

def download_SP_500_data():
    symb = open("symbol_list.txt", "r")
    list_symb = symb.readlines()
    symb_list = [i.strip() for i in list_symb]
    symb.close()
    #print(symb_list)
    #print(symb)



    # initialize parameters
    start_date = datetime(2017, 1, 1)
    end_date = datetime(2022, 12, 15)

    #for i in range(1):
    #for i in range(len(symb_list)):
        # open the file in the write mode
        #file_name = 'stock_data/'+"META"+'_data.csv'
        file_name = "stock_data/"+symb_list[i]+'_data.csv'
        f = open(file_name, 'w')

        #data = yf.download("META", start = start_date, end = end_date)
        data = yf.download(symb_list[i], start = start_date, end = end_date)
        df = pd.DataFrame(data)
        df.to_csv(file_name)
        #print(data)
        #print(df.head())

        print("done") 
        
download_SP_500_data()