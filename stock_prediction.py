from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline
import numpy as np
from matplotlib.pylab import rcParams
rcParams['figure.figsize']=20,10
from keras.models import Sequential
from keras.layers import LSTM,Dropout,Dense
from sklearn.preprocessing import MinMaxScaler
from pandas import Timestamp
from sklearn.metrics import mean_squared_error, mean_absolute_error

from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline
import numpy as np
from matplotlib.pylab import rcParams
rcParams['figure.figsize']=20,10
from keras.models import Sequential
from keras.layers import LSTM,Dropout,Dense
from sklearn.preprocessing import MinMaxScaler
from pandas import Timestamp
from sklearn.metrics import mean_squared_error, mean_absolute_error

def stock_closing_price_prediction(stock_symbol):
    data_path = "stock_data/"+str(stock_symbol.upper())+"_data.csv"
    df=pd.read_csv(data_path)
    
    
    df["Date"]=pd.to_datetime(df.Date,format="%Y-%m-%d")
    df.index=df['Date']

    plt.figure(figsize=(16,8))
    plt.plot(df["Close"])
    plt.title("Close Price history")
    plt.xlabel("Time")
    plt.ylabel("Price")
    #plt.ioff()
    
    print("Actual prices - Plot before prediction")
    plt.show()
    #print(df.head())
    
    file=df.sort_index(ascending=True,axis=0)
    new_frame=pd.DataFrame(index=range(0,len(df)),columns=['Date','Close'])

    for i in range(0,len(file)):
        new_frame["Date"][i]=(file['Date'][i])
        new_frame["Close"][i]=file["Close"][i]

    data_scaler=MinMaxScaler(feature_range=(0,1))
    final_dataframe=new_frame.values

    #print(final_dataframe)

    train_values=final_dataframe[0:987,:]
    final_values=final_dataframe[987:,:]

    #print(final_values)
    new_frame.index=new_frame.Date
    new_frame.drop("Date",axis=1,inplace=True)

    data_scaler=MinMaxScaler(feature_range=(0,1))
    #a = Timestamp("2017-01-03")
    #print(a)
    
    #scaled_values=scaler.fit_transform(final_dataframe)
    scaled_values=data_scaler.fit_transform(new_frame)
    #print(scaled_values)
    x_train_values,y_train_values=[],[]

    for i in range(60,len(train_values)):
        x_train_values.append(scaled_values[i-60:i,0])
        y_train_values.append(scaled_values[i,0])

    x_train_values,y_train_values=np.array(x_train_values),np.array(y_train_values)

    x_train_values=np.reshape(x_train_values,(x_train_values.shape[0],x_train_values.shape[1],1))
    
    
    X_test=[]
    for i in range(60,inputs_values.shape[0]):
        X_test.append(inputs_values[i-60:i,0])
    X_test=np.array(X_test)

    X_test=np.reshape(X_test,(X_test.shape[0],X_test.shape[1],1))
    closing_price_pediction=training_model.predict(X_test)
    closing_price_pediction=data_scaler.inverse_transform(closing_price_pediction)
    
    
    train_values=new_frame[:987]
    
    final_values=new_frame[987:]
    final_values['Predictions']=closing_price_pediction


    #plt.xlabel(new_dataset["Date"])
    plt.plot(train_values["Close"])
    plt.plot(final_values[['Close',"Predictions"]])
    plt.title("Close Price history")
    #plt.xticks(range(0,len(df.index)), df.index)
    plt.xlabel("Time")
    plt.ylabel("Price")
    
    #Evaluation the model
    print('mean_squared_error : ', mean_squared_error(valid_data['Close'],valid_data["Predictions"] ))
    print('mean_absolute_error : ', mean_absolute_error(valid_data['Close'],valid_data["Predictions"]))
    
stock_closing_price_prediction(stock_symbol)